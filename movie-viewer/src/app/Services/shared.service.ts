import { Injectable } from '@angular/core';
import { Movie } from '../Models/movie';
import { Genre } from '../Models/genre';

@Injectable({
    providedIn: 'root'
})
export class SharedService {
    protected _genres: Genre[];
    protected _listData: Movie[];

    get genres() {
        return this._genres;
    }

    set genres(value: Genre[]) {
        this._genres = value;
    }
    get listData() {
        return this._listData;
    }

    set listData(value: Movie[]) {
        this._listData = value;
    }


    constructor() { }
}
