import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Response } from '../models/response';
import { Movie } from '../Models/movie';

@Injectable({
    providedIn: 'root'
})

export class DataService {
    protected static API_KEY = '9198fa6d9a9713bc6b03ee9582525917';
    protected static DOMAIN = "http://api.themoviedb.org/";
    protected static MOVIES_API = DataService.DOMAIN + '3/discover/movie/';
    protected static SEARCH_MOVIES_API = DataService.DOMAIN + '3/search/movie/';
    protected static FIND_MOVIE_API = DataService.DOMAIN + '3/movie/';
    protected static GENRES_API = DataService.DOMAIN + '3/genre/movie/list'
    protected static IMAGES_API = 'https://image.tmdb.org/t/p/{}/';
    protected static SMALL_IMAGE_TYPE = "w200";
    protected static NORMAL_IMAGE_TYPE = "w300";
    protected static IMG_OPTIONS: Object = {
        responseType: 'blob',
        params: {
            api_key: DataService.API_KEY
        }
    }

    constructor(protected http: HttpClient) { }

    public search(query: string): Observable<Response> {
        return this.http.get<Response>(DataService.SEARCH_MOVIES_API, {
            params: {
                api_key: DataService.API_KEY,
                query: query,
                //year: 2019
            }
        });
    }

    public get(id: number): Observable<Movie> {
        return this.http.get<Movie>(DataService.FIND_MOVIE_API + id, {
            params: {
                api_key: DataService.API_KEY
            }
        });
    }

    public getAllMovies(page: number): Observable<Response> {
        return this.http.get<Response>(DataService.MOVIES_API, {
            params: {
                page: page.toString(),
                sort_by: "popularity.desc",
                api_key: DataService.API_KEY
            }
        });
    }

    public getAllGenres(): Observable<Response> {
        return this.http.get<Response>(DataService.GENRES_API, {
            params: {
                language: "en-US",
                api_key: DataService.API_KEY
            }
        });
    }

    public getSmallImage(imageCode: string): Observable<Blob> {
        return this.getImage(imageCode, DataService.SMALL_IMAGE_TYPE);
    }

    public getNormalImage(imageCode: string): Observable<Blob> {
        return this.getImage(imageCode, DataService.NORMAL_IMAGE_TYPE);
    }

    protected getImage(imageCode: string, type: string): Observable<Blob> {
        return this.http.get<Blob>(DataService.IMAGES_API.replace("{}", type) + imageCode,
            DataService.IMG_OPTIONS);
    }
}
