import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MovieListComponent } from './Components/movie-list/movie-list.component';
import { MovieViewComponent } from './Components/movie-view/movie-view.component';


const routes: Routes = [
    { path: '', redirectTo: '/movie-list', pathMatch: 'full' },
    {
        path: 'movie-list',
        component: MovieListComponent
    },
    {
        path: 'movie-view/:id',
        component: MovieViewComponent
    }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
