import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HighchartsChartModule } from 'highcharts-angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClarityModule } from '@clr/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MovieListComponent } from './Components/movie-list/movie-list.component';
import { MovieViewComponent } from './Components/movie-view/movie-view.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { GenreListComponent } from './Components/genre-list/genre-list.component';
import { MovieRatingComponent } from './Components/movie-rating/movie-rating.component';
import { MovieDurationPipe } from './Pipes/movie-duration.pipe';

@NgModule({
    declarations: [
        AppComponent,
        MovieListComponent,
        MovieViewComponent,
        GenreListComponent,
        MovieRatingComponent,
        MovieDurationPipe
    ],
    imports: [
        FormsModule,
        BrowserModule,
        AppRoutingModule,
        ClarityModule,
        BrowserAnimationsModule,
        HighchartsChartModule,
        HttpClientModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
