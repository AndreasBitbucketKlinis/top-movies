import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'movieDuration'
})
export class MovieDurationPipe implements PipeTransform {

    transform(value: number): string {
        const hours = (value / 60);
        const hoursRnd = Math.floor(hours);
        const minutes = (hours - hoursRnd) * 60;
        const minutesRnd = Math.round(minutes);

        return hoursRnd + "h " + minutesRnd + "min";
    }

}
