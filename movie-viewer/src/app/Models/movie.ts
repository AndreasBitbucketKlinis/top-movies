import { SafeUrl } from '@angular/platform-browser';
import { Genre } from './genre';

export class Movie {
    id: number
    homepage: string;
    title: string;
    overview: string;
    poster_path: string;
    release_date: Date;
    tagline: string;
    vote_average: number;
    backdrop_path: string;
    genres: Genre[];
    genre_ids: number[];
    image: SafeUrl;
}
