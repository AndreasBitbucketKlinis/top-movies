import { Movie } from './movie';
import { Genre } from './genre';

export class Response {
    page: number
    total_results: number;
    total_pages: number;
    results: Movie[];
    genres: Genre[];
}
