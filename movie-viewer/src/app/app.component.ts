import { Component, ViewChild, ElementRef } from '@angular/core';
import { Movie } from './Models/movie';
import { debounce } from 'rxjs/operators';
import { fromEvent, interval } from 'rxjs';
import { DataService } from './Services/data.service';
import { Router } from '@angular/router';
import { chart } from 'highcharts';
import * as Highcharts from 'highcharts';
import { SharedService } from './Services/shared.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    @ViewChild('autocomplete', { static: false }) autocomplete: ElementRef;
    @ViewChild('charttarget', { static: false }) chartTarget: ElementRef;

    protected selectedMovieText = "";
    protected stitle = 'movie-viewer';
    protected movies: Movie[] = [];
    protected selectedMovie: Movie = new Movie();
    protected selectedMovieId: number = -1;
    protected modalOpened = false;
    protected chart: Highcharts.Chart;

    constructor(protected router: Router, protected dataService: DataService, protected sharedService: SharedService) { }

    protected ngAfterViewInit(): void {
        const clicks = fromEvent(this.autocomplete.nativeElement, 'input');
        const result = clicks.pipe(debounce(() => interval(1000)));
        result.subscribe((e: Event) => {
            this.dataService.search((<HTMLInputElement>e.target).value).subscribe(data => {
                this.movies = data.results;
            });
        });
    }

    protected onChange(): void {
        if (this.movies.length) {
            let selected = this.movies.find((c) => c.title == this.selectedMovieText);
            if (selected) {
                this.selectedMovie = selected;
                this.selectedMovieId = this.selectedMovie.id;
            }
            else this.selectedMovieId = -1;
        }

    }

    protected navigateToMovie(): void {
        if (this.selectedMovieId == -1) return;
        this.router.navigate(['/movie-view', this.selectedMovieId]);
    }

    protected openBarChart(): void {
        const data = this.sharedService.listData;
        const genres = this.sharedService.genres;

        let categoryAppearences = {};
        for (let i = 0; i < data.length; i++)
            for (let j = 0; j < data[i].genre_ids.length; j++) {
                let category = genres.find(g => g.id == data[i].genre_ids[j]).name;
                if (!(category in categoryAppearences)) {
                    categoryAppearences[category] = 1;
                } else categoryAppearences[category] += 1;
            }

        let categories: string[] = Object.keys(categoryAppearences);
        let chartData: number[] = Object.values(categoryAppearences);
        this.modalOpened = true;

        setTimeout(() => {
            let options = {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Top Genres'
                },
                xAxis: {
                    categories: categories
                },
                yAxis: {
                    minRange: 1
                },
                credits: {
                    enabled: false
                },
                exporting: { enabled: false },
                series: [{
                    name: 'Frequence',
                    data: chartData
                }]
            } as Highcharts.Options;
            this.chart = chart(this.chartTarget.nativeElement, options);
        });
    }
}
