import { Component, OnInit, Input } from '@angular/core';
import { Genre } from '../../Models/genre';

@Component({
    selector: 'app-genre-list',
    templateUrl: './genre-list.component.html',
    styleUrls: ['./genre-list.component.css']
})
export class GenreListComponent implements OnInit {
    @Input() genreIds: number[];
    @Input() allGenres: Genre[];
    @Input() genres: Genre[];

    constructor() { }

    public ngOnInit() {
        if (!this.genres) {
            this.genres = [];
            for (let i = 0; i < this.genreIds.length; i++)
                this.genres.push(this.allGenres.find(d => d.id == this.genreIds[i]))
        }
    }

    protected showGenreLabels(): string {
        let labelsStr = "";
        const labelTemplate = "<span class='label label-info'>{}</span>";
        for (let i = 0; i < this.genres.length; i++)
            labelsStr += labelTemplate.replace("{}", this.genres[i].name);

        return labelsStr;
    }
}
