import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Movie } from '../../Models/movie';
import { DataService } from '../../Services/data.service';
import { Genre } from '../../models/genre';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'app-movie-view',
    templateUrl: './movie-view.component.html',
    styleUrls: ['./movie-view.component.css']
})
export class MovieViewComponent implements OnInit {

    protected id: any;
    protected movie: Movie;
    protected pageNotLoaded = true;
    protected runtime: string;

    constructor(
        protected route: ActivatedRoute,
        protected router: Router,
        protected dataService: DataService,
        protected sanitizer: DomSanitizer) {
    }

    public ngOnInit() {
        this.route.params.subscribe(params => {
            this.id = params['id'];
            if (this.id) {
                this.dataService.get(this.id).subscribe((movie: Movie) => {
                    if (movie) {
                        this.dataService.getNormalImage(movie.poster_path).subscribe(data => {
                            movie.image = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(data));
                            this.movie = movie;
                            this.pageNotLoaded = false;
                        });
                    }
                    else {
                        this.pageNotLoaded = true;
                        console.log("Movie with id '${id}' not found, returning to list");
                        this.gotoList();
                    }
                });
            }
        });
    }

    protected showGenreLabels(genres: Genre[]): string {
        let labelsStr = "";
        const labelTemplate = "<span class='label label-info'>{}</span>";
        for (let i = 0; i < genres.length; i++)
            labelsStr += labelTemplate.replace("{}", genres[i].name);
        return labelsStr;
    }

    private gotoList(): void {
        this.router.navigate(['/movie-list']);
    }
}
