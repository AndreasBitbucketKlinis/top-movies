import { Component, OnInit } from '@angular/core';
import { ClrDatagridStateInterface } from "@clr/angular";
import { Movie } from '../../Models/movie';
import { DataService } from '../../Services/data.service';
import { Genre } from '../../models/genre';
import { DomSanitizer } from '@angular/platform-browser';
import { forkJoin, Observable } from 'rxjs';
import { SharedService } from '../../Services/shared.service';

@Component({
    selector: 'app-movie-list',
    templateUrl: './movie-list.component.html',
    styleUrls: ['./movie-list.component.css']
})

export class MovieListComponent implements OnInit {
    protected movies: Movie[];
    protected genres: Genre[];
    protected moviesCount = 0;
    protected loading = true;
    protected pageSize = 10;
    protected lastPageIndex = 0;

    constructor(protected dataService: DataService, protected sharedService: SharedService,
        protected sanitizer: DomSanitizer) { }

    public ngOnInit() {
        let datagrids = document.getElementsByClassName("datagrid") as HTMLCollectionOf<HTMLElement>;
        datagrids[0].style.overflow = "hidden";
        this.dataService.getAllGenres().subscribe(data => {
            this.genres = data.genres;
            this.sharedService.genres = this.genres;
        });
    }

    protected refresh(state: ClrDatagridStateInterface): void {
        this.lastPageIndex = this.moviesCount / this.pageSize;

        this.loading = true;
        let databasePage;

        if (state.page.to == -1) databasePage = 1;
        else {
            const previousSize = this.pageSize;
            this.pageSize = state.page.size;
            if (previousSize != this.pageSize) {
                this.refresh(state);
                return;
            }

            const divResult = state.page.to / 20;
            databasePage = Math.ceil(divResult == 1 ? 2 : divResult);
        }

        this.dataService.getAllMovies(databasePage).subscribe(data => {
            let moviesForUpdate: Movie[] = this.calculateMoviesShown(data, state);

            let imageActions: Observable<Blob>[] = [];
            for (let i = 0; i < moviesForUpdate.length; i++)
                if (moviesForUpdate[i].backdrop_path)
                  imageActions.push(
                      this.dataService.getSmallImage(moviesForUpdate[i].backdrop_path));

            forkJoin(imageActions).subscribe(results => {
                for (let j = 0; j < results.length; j++)
                    moviesForUpdate[j].image = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(results[j]));

                this.movies = moviesForUpdate;
                this.sharedService.listData = this.movies;
                this.loading = false;
            });            
        });
    }

    protected calculateMoviesShown(data: import("C:/Users/Andreas/source/repos/movie-viewer/src/app/models/response").Response, state: ClrDatagridStateInterface<any>) {
        let moviesForUpdate: Movie[];
        if (!this.moviesCount) {
            this.moviesCount = data.total_results;
            this.lastPageIndex = this.moviesCount / this.pageSize;
            moviesForUpdate = data.results.slice(0, 10);
        }
        else {
            let modResult: number;
            switch (this.pageSize) {
                case 5:
                    modResult = state.page.to % 20;
                    if (modResult == 4)
                        moviesForUpdate = data.results.slice(0, 5);
                    else if (modResult == 9)
                        moviesForUpdate = data.results.slice(5, 10);
                    else if (modResult == 14)
                        moviesForUpdate = data.results.slice(10, 15);
                    else
                        moviesForUpdate = data.results.slice(15, 20);
                    break;
                case 10:
                    modResult = state.page.to % 20;
                    if (modResult == 9)
                        moviesForUpdate = data.results.slice(0, 10);
                    else
                        moviesForUpdate = data.results.slice(10, 20);
                    break;
                default:
                    moviesForUpdate = data.results;
            }
        }
        return moviesForUpdate;
    }
}
