import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-movie-rating',
    templateUrl: './movie-rating.component.html',
    styleUrls: ['./movie-rating.component.css']
})
export class MovieRatingComponent implements OnInit {
    @Input() value: number;

    protected checkedStarsNumber: number[] = [];
    protected starsNumber: number[] = [];

    constructor() { }

    public ngOnInit() {
        this.checkedStarsNumber = new Array(Math.round(this.value));
        this.starsNumber = new Array(10 - this.checkedStarsNumber.length);
    }

}
